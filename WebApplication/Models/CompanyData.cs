﻿using System;

namespace WebApplication.Models
{
    public class CompanyData
    {
        public string companyName { get; set; }
        public string companyVATNumber { get; set; }
        public string priority { get; set; }
        public string companyStreet { get; set; }
        public string companyZip { get; set; }
        public string companyState { get; set; }
        public string companyRegion { get; set; }
        public string companyEmail { get; set; }
        public string companyPhone { get; set; }
        public string contactPersonName { get; set; }
        public string contactPersonPhone { get; set; }
        public string contactPersonEmail { get; set; }
        public DateTime dateOfSubmite { get; set; }
    }
}