﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using WebApplication.Models;

namespace WebApplication.Helpers
{
    public class JsonRepository
    {
        private string _nameJsonFile;

        public JsonRepository(string nameJsonFile)
        {
            _nameJsonFile = nameJsonFile;
            
            bool exists = System.IO.File.Exists(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["filePath"] + _nameJsonFile + ".json"));
            if (!exists)
            {
                using (StreamWriter _jsFile = new StreamWriter(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["filePath"] + _nameJsonFile + ".json"), true))
                {
                    _jsFile.WriteLine("[]"); 
                }
            }
        }
        public string ReadJson()
        {
            string[] lines = File.ReadAllLines(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["filePath"] + _nameJsonFile + ".json"));
            return String.Join("", lines);
        }
        public void AddItem(CompanyData data)
        {
            string stringJson = ReadJson();
            List<CompanyData> json = JsonConvert.DeserializeObject<List<CompanyData>>(stringJson);
            json.Add(data);
            using (StreamWriter file = File.CreateText(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["filePath"] + _nameJsonFile + ".json")))
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(file, json);
            }
        }
        public string[] GetAllCompanyName()
        {
            string stringJson = ReadJson();

            List<CompanyData> json = JsonConvert.DeserializeObject<List<CompanyData>>(stringJson);

            List<List<CompanyData>> CompanyDatas = new List<List<CompanyData>>();

            var groupByName = json.GroupBy(t => t.companyName);

            List<string> companyNames = new List<string>();

            foreach (var item in groupByName)
            {
                List<CompanyData> orderedData = item.OrderBy(t => t.dateOfSubmite).ToList();
                orderedData.Reverse();
                List<CompanyData> tmp = new List<CompanyData>();

                if (orderedData.Count() > 3)
                {
                    for (int i = 0; i < 3; i++)
                    {
                        tmp.Add(orderedData.ElementAt(i));
                    }
                }
                else
                {
                    tmp = orderedData;
                }

                foreach (var companyDataItem in tmp)
                {
                    companyNames.Add(String.Format("{0} ({1})", companyDataItem.companyName, companyDataItem.dateOfSubmite));
                }
            }
            return companyNames.ToArray();
        }
        public CompanyData GetCompanyDataByName(string name)
        {
            Regex rgx = new Regex(" (.+)");
            name = rgx.Replace(name, "");
            string stringJson = ReadJson();
            List<CompanyData> json = JsonConvert.DeserializeObject<List<CompanyData>>(stringJson);
            json.Reverse();
            CompanyData companyData = json.First(d => d.companyName == name);
            return companyData;
        }
    }
}
