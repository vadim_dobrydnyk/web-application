﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication.Helpers
{
    public class GetQuartal
    {
        public static int Get()
        {
            if (DateTime.Now.Month <= 3) { return 1; }
            else if (DateTime.Now.Month > 3 && DateTime.Now.Month <= 6) { return 2; }
            else if (DateTime.Now.Month > 6 && DateTime.Now.Month <= 9) { return 3; }
            else { return 4; }
        }
    }
}