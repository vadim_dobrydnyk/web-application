﻿using System;
using System.Web.Mvc;
using System.Configuration;
using WebApplication.Models;
using WebApplication.Helpers;

namespace WebApplication.Controllers
{
    public class HomeController : Controller
    {
        private readonly JsonRepository _jsonRepository;

        public HomeController()
        {
            _jsonRepository = new JsonRepository(ConfigurationManager.AppSettings["nameJsonFile"]);
        }

        public ActionResult Index()
        {
            ViewBag.NameCompany = _jsonRepository.GetAllCompanyName();
            ViewBag.Month = DateTime.Now.Month;
            ViewBag.Quartal = GetQuartal.Get();
            ViewBag.CompanyData = new CompanyData();
            return View();
        }
        [HttpPost]
        public ActionResult Submite(string CompanyName, string CompanyVATNumber)
        {
            ViewBag.NameCompany = _jsonRepository.GetAllCompanyName();
            var exelfile = Request.Files[0];
            if (exelfile == null || exelfile.ContentLength == 0)
            {
                TempData["Error"] = "Please select a exel file";
                return RedirectToAction("Index", "Home");
            }
            else
            {
                if (exelfile.FileName.EndsWith("xls") || exelfile.FileName.EndsWith("xlsx"))
                {
                    string path = Server.MapPath(ConfigurationManager.AppSettings["filePath"] + exelfile.FileName);

                    string[] arr = exelfile.FileName.Split('.');
                    arr[0] = String.Format("{0}-{1}-{2}-{3}-{4}-{5}-{6}", DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, CompanyVATNumber);
                    exelfile.SaveAs(path);

                    System.IO.File.Move(path, Server.MapPath(ConfigurationManager.AppSettings["filePath"] + arr[0] + "." + arr[1]));

                    CompanyData data = new CompanyData();

                    data.companyName = Request.Form["companyName"];
                    data.companyVATNumber = Request.Form["companyVATNumber"];
                    data.priority = Request.Form["priority"];
                    data.companyStreet = Request.Form["companyStreet"];
                    data.companyZip = Request.Form["companyZip"];
                    data.companyState = Request.Form["companyState"];
                    data.companyRegion = Request.Form["companyRegion"];
                    data.companyEmail = Request.Form["companyEmail"];
                    data.companyPhone = Request.Form["companyPhone"];
                    data.contactPersonName = Request.Form["contactPersonName"];
                    data.contactPersonPhone = Request.Form["contactPersonPhone"];
                    data.contactPersonEmail = Request.Form["contactPersonEmail"];
                    data.dateOfSubmite = DateTime.Now;
                    _jsonRepository.AddItem(data);
                    ViewBag.NameCompany = _jsonRepository.GetAllCompanyName();
                    ViewBag.Month = DateTime.Now.Month;
                    ViewBag.Quartal = GetQuartal.Get();
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    TempData["Error"] = "File type is incorrect";
                    return RedirectToAction("Index", "Home");
                }
            }
        }
        [HttpPost]
        public ActionResult AutoComplete(string companyName)
        {
            CompanyData companyData = _jsonRepository.GetCompanyDataByName(companyName);
            ViewBag.Month = DateTime.Now.Month;
            ViewBag.Quartal = GetQuartal.Get();
            ViewBag.NameCompany = _jsonRepository.GetAllCompanyName();
            return PartialView("FormPartialView", companyData);
        }
    }
}